<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">

<link rel="top" href="../" title="SeaMonkey Project">
<link rel="up" href="./" title="Documentation &amp; Help">

<title>SeaMonkey Sync</title>
</head>
<body>

<h1>SeaMonkey Sync</h1>

<div class="important">
  <strong class="shout">Attention!</strong> Starting with Firefox 29
  (SeaMonkey 2.26), it is not possible anymore to set up legacy Sync
  accounts as described below. The new Sync back-end, which uses Firefox
  Accounts, is not supported by SeaMonkey yet. If you want to use Sync with
  both Firefox and SeaMonkey, you will need to use an older version (e.g.
  Firefox 28 or SeaMonkey 2.25) to do the setup. Once Sync setup is
  complete, you may go back to using the current versions.
</div>

<h2 id="about">About SeaMonkey Sync</h2>
<div class="section">
  <p>Sync lets you synchronize your browsing data such as bookmarks, history
    and passwords between installations of SeaMonkey or Firefox (e.g. your
    computers at home and at work) or mobile phones that support Firefox Sync.</p>
  <p>All data is stored securely in a way that allows only you to access it
    (encryption happens on the client side). Mozilla provides a default Sync
    server free of charge, but individuals can <a
    href="http://docs.services.mozilla.com/howtos/run-sync.html">run their
    own</a>, too.</p>
  <p>Sync uses a set of credentials: A user name (email address), a password,
    and a Sync Key. The user name and password are used for communication
    between clients (browsers using a certain Sync account) and the Sync
    server, i.e. the password protects you from others that might try to
    take over your account. The Sync Key on the other hand is used to
    encrypt all data before it is sent to the Sync server. Unlike the
    user name and password, which are known to the Sync server, it is only
    known to you.</p>
  <p>After the initial setup, Sync operates in the background, continually
    and automatically synchronizing your data with the Sync server (manually
    triggering a synchronization is possible, too). Clients that run
    simultaneously and access the same Sync account have their data merged
    on the fly.</p>
</div>

<h2 id="setup">Setting Up Sync</h2>
<div class="section">
  <ol>
    <li>Open the "Tools" menu from a SeaMonkey browser or MailNews
      window and select "Set Up Sync&hellip;".</li>
    <li>Click "Create a New Account".</li>
    <li>If you haven't done so already, you will be prompted to set up
      a Master Password. You need to choose one in order to have the
      Sync credentials stored securely on your local machine.</li>
    <li>On the following screen (Account Details), enter your email
      address, create a password and confirm it. Then select "<em>I
      agree to the Terms of Service and Privacy Policy</em>" and
      click "Next".</li>
    <li>Next you will be presented with the Sync Key (Recovery Key)
      for the Sync account you just created. Save or print a copy of
      it and store it somewhere safe (if you lose it, the data on the
      Sync server is irretrievably lost!). Afterwards, click "Next".</li>
    <li>Now confirm that you are not a robot by entering the text from
      the image. Then click "Next".</li>
    <li>You will see a <em>Setup Complete</em> message. Click "Finish"
      to close the setup window. Further customization can be done under
      Preferences/Sync.</li>
    <li>You do not need to do anything else. SeaMonkey Sync will
      automatically synchronize your data in the background from here
      on. If you wish, you can manually trigger a Sync run at any time using
      either the Tools menu entry "Sync Now" that now appears instead of
      "Set Up Sync", or a Sync button that can be added to any customizable
      toolbar.<br>
      Note: The first synchronization should happen quickly but may
      take more than 30 minutes depending on how much information
      needs to be synced.</li>
  </ol>
</div>

<h2 id="adddevice">Adding a Device</h2>
<div class="section">
  <p>The following assumes that you already set up Sync on at least one
    device (see steps above) and have both devices at hand.</p>
  <ol>
    <li>On the device with Sync already set up, go to Preferences/Sync and
      choose "Add a Device" from "Manage Account". A new window will open.</li>
    <li>If your second device is another instance of SeaMonkey, open the
      "Tools" menu from a SeaMonkey browser or MailNews window and select
      "Set Up Sync", then click "Connect". Otherwise follow the steps
      outlined in the <a
      href="http://support.mozilla.com/en-US/kb/find-code-to-add-device-to-firefox-sync">Where
      can I find the code to add a device to Firefox Sync</a> article on
      Firefox Help.</li>
    <li>Enter the code as it is displayed on your second device. Then click
      "Next".</li>
    <li>When the code is accepted, you will see a <em>Device Connected</em>
      message and your device will immediately begin syncing. Click "Finish"
      to close the setup window and go back to the Sync panel.</li>
  </ol>
  <p>Alternatively, if you only have your second device at hand, you can set
    it up using the Sync account information (email address, password and
    Sync Key / Recovery Key). Following are the steps you need to take
    in order to set up another instance of SeaMonkey. For devices running
    Firefox, see the <a
    href="http://support.mozilla.com/en-US/kb/add-a-device-to-firefox-sync#w_how-do-i-add-a-device-when-im-not-near-my-computer">How
    do I add a device to Firefox Sync</a> article on Firefox Help.</p>
  <ol>
    <li>On the second device, open the "Tools" menu from a SeaMonkey browser
      or MailNews window and select "Set Up Sync".</li>
    <li>Click "Connect".</li>
    <li>Click "I don't have the device with me" below the three boxes.</li>
    <li>Enter your Sync email address and password, then click "Next".</li>
    <li>Enter your Sync Key (Recovery Key) and click "Next".</li>
    <li>You will see a <em>Setup Complete</em> message. Click "Finish" to
      close the setup window.</li>
  </ol>
</div>

<h2 id="accountinfo">Finding Sync Account Information</h2>
<div class="section">
  <p>To determine your Sync account information, follow these steps:</p>
  <ol>
    <li>Go to Preferences/Sync.</li>
    <li>The account name is displayed at the top. You cannot change it.</li>
    <li>The computer name (which is used for the "Tabs From Other Computers"
      functionality available from the "Go" menu of browser windows)
      is displayed at the bottom. This string can be changed.</li>
    <li>Your password is not displayed anywhere. You can change it using
      "Manage Account" / Change your Password", though.</li>
    <li>The Sync Key (Recovery Key) is displayed if you choose "Manage Account"
      / "My Sync Key" ("My Recovery Key") or "Add a Device" followed by
      clicking "I don't have the device with me".</li>
  </ol>
</div>

</body>
</html>
